import cv2

img = cv2.imread('./images/book.png')
cv2.imshow('original',img)

retval, threshold_Color = cv2.threshold(img,12,255,cv2.THRESH_BINARY)
cv2.imshow('threshold Color',threshold_Color)

grayscaled = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

retval, threshold_Gray = cv2.threshold(grayscaled,8,255,cv2.THRESH_BINARY)
cv2.imshow('threshold gray',threshold_Gray)

thr_GU = cv2.adaptiveThreshold(grayscaled,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,115,1)
cv2.imshow('Gaussian', thr_GU)

thr_mean = cv2.adaptiveThreshold(grayscaled,255,cv2.ADAPTIVE_THRESH_MEAN_C,cv2.THRESH_BINARY,115,1)
cv2.imshow('Mean',thr_mean)

cv2.waitKey(0)