import cv2
import numpy as np


img_source = cv2.imread('./images/raspberrypicluster.jpg')

#img_source.astype(np.uint8)
img_source_gray = cv2.cvtColor(img_source,cv2.COLOR_BGR2GRAY)
#print(img_source_gray.dtype)

img_template = cv2.imread('./images/rca.jpg')

w, h = img_template.shape[:-1]

res = cv2.matchTemplate(img_source, img_template, cv2.TM_CCOEFF_NORMED)
threshold = 0.7
loc = np.where( res >= threshold)

for pt in zip(*loc[::-1]):
    cv2.rectangle(img_source, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 2)

cv2.imshow('detected', img_source)
cv2.waitKey(0)

